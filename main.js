const navbar = document.querySelector("#navbar")

document.addEventListener("scroll", ()=> {
    const scrolled = window.pageYOffset

    if(scrolled > 100) {
        navbar.classList.add("shadow")
    } else {
        navbar.classList.remove("shadow")
    }
   
})